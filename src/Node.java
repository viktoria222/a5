import java.util.HashMap;
//https://github.com/egertaia/AlgorithmAndDataStructure/blob/7eb4257b89416a5ea4f032d9ffa1bc21eb67e4cf/home5/src/Node.java


public class Node {
    private String name;
    private Node firstChild;
    private Node nextSibling;
    static int index = 0;
    static HashMap<String, String> nodes = new HashMap<>();
    static String xName = "X";
    static int xlength = xName.length() + 2;

    Node(String stringForm) {
        int leftClose = stringForm.indexOf(")");
        String operands = stringForm.substring(1, leftClose);
        String name = stringForm.substring(leftClose + 1, stringForm.length());
        this.name = name;
        String value[] = operands.split("[,]");
        if (value[0].isEmpty())
            throw new RuntimeException(" wrong operands");
        else {
            Node newNode = null;
            Node oldNode = null;
            for (int i = value.length - 1; i >= 0; i--) {
                if (value[i].isEmpty())
                    throw new RuntimeException("operand is null");
                newNode = new Node(value[i], null, null);
                newNode.setNextSibling(oldNode);
                oldNode = newNode;
            }
            setFirstChild(newNode);
        }

    }

    Node(String name, Node d, Node r) {
        this.name = name;
        this.firstChild = d;
        this.nextSibling = r;
    }

    public void setFirstChild(Node firstChild) {
        this.firstChild = firstChild;
    }

    public void setNextSibling(Node nextSibling) {
        this.nextSibling = nextSibling;
    }

    public static Node parsePostfix(String s) {
        String s1 = s;
        while (s1.contains("(") || s1.contains(")") || s1.contains(",") || s1.contains(" ")) {
            s1 = parse(s1);
        }
        Node node;
        if (s1.contains(xName)) {
            node = new Node(nodes.get(s1));
            nodes.remove(s1);
            changeNodes(node);
        } else {
            s1 = s1.replace("\t", "");
            if (s1.length() == 0)
                throw new RuntimeException(" invalid operand" + s);
            node = new Node(s1, null, null);
        }
        return node;
    }

    public static String parse(String s) {
        int leftOpen = s.indexOf("(");//(x,y)A
        int rightOpen = s.lastIndexOf("(");
        String rightSide = s.substring(leftOpen + 1, s.length());
        if (rightSide.length() < 3)
            throw new RuntimeException(" wrong count of operands" + s);
        if (leftOpen == rightOpen) {
            int leftClose = rightSide.indexOf(")");
            if (leftClose < 0)
                throw new RuntimeException(" wrong count of parenthesis" + s);
            String operands = rightSide.substring(0, leftClose);
            String name = rightSide.substring(leftClose + 1, rightSide.length());
            int last = name.indexOf(")");
            int last2 = name.indexOf(",");
            if (last < 0) last = name.length();
            if ((last2 > 0) && (last2 < last)) last = last2;
            name = name.substring(0, last);

            nodes.put(xName + String.format("%02d", index), "(" + operands + ")" + name);
            String restString = s.substring(0, leftOpen) + xName + String.format("%02d", index);
            restString += rightSide.substring(rightSide.indexOf(name) + name.length(), rightSide.length());
            index++;
            return restString;
        } else {
            String res = parse(rightSide);
            return "(" + s.substring(0, leftOpen) + res;
        }
    }

    static void changeNodes(Node node) {
        Node curNode = node;
        String key = "";
        Node second = curNode.nextSibling;
        if (second != null) {
            key = second.name;
            if (key.contains(xName)) {
                Node newNode = new Node(nodes.get(key));
                if (second.firstChild != null) newNode.setFirstChild(second.firstChild);
                if (second.nextSibling != null) newNode.setNextSibling(second.nextSibling);
                curNode.nextSibling = newNode;
                nodes.remove(key);
            }
        }
        Node first = curNode.firstChild;
        if (first != null) {
            key = first.name;
            if (key.contains(xName)) {
                Node newNode = new Node(nodes.get(key));
                if (first.firstChild != null) newNode.setFirstChild(first.firstChild);
                if (first.nextSibling != null) newNode.setNextSibling(first.nextSibling);
                curNode.firstChild = newNode;
                nodes.remove(key);

            }
        }
        if (curNode.firstChild != null)
            changeNodes(curNode.firstChild);
        if (curNode.nextSibling != null)
            changeNodes(curNode.nextSibling);

    }

    public String leftParentheticRepresentation() {
        StringBuilder s = new StringBuilder();
        s.append(name);
        if (null == firstChild) ;
        else {
            s.append("(");
            s.append(firstChild.leftParentheticRepresentation());
            s.append(")");
        }
        if (null == nextSibling) ;
        else {
            s.append(",");
            s.append(nextSibling.leftParentheticRepresentation());
        }
        return s.toString();
    }


    public static void main(String[] param) {


        String s = "((A,(B1,B2)B)C,(B,C)D)Y1";
        String s1 = "(B)A,(D)C";


        Node t = Node.parsePostfix(s1);


        String v = t.leftParentheticRepresentation();


        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C) (B1,(X,Y)C)A ==> A(B1,C)
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(name);
        if (null == firstChild) ;
        else {
            s.append("(");
            s.append(firstChild.toString());
            s.append(")");
        }
        if (null == nextSibling) ;
        else {
            s.append(",");
            s.append(nextSibling.toString());
        }
        return s.toString();
    }
}
